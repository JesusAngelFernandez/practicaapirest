#importamos node
FROM node

#establecemos el directorio de trabajo
WORKDIR /apitechu

#añadimos el directorio de trabajo
ADD . /apitechu

#exponemos el puerto de trabajo, que es donde abre el node que es el 3000.
EXPOSE 3000

CMD ["npm", "start"]

var express = require('express');
var port = process.env.PORT || 3000;
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());


var baseMLabURL = "https://api.mlab.com/api/1/databases/apitechu_jaf/collections/";
var mLabAPIKey = "apiKey=_T5XQES_fRSsUfre0ShTLFfnjdH86-rH";
var requestJson = require('request-json');

//apikey={{ENV}}
app.listen(port);
console.log("API escuchando en el puerto " + port);

app.get("/apitechu/v1",
  function(req, res) {
    console.log("GET /apitechu/v1");
    res.send({"msg" : "Hola desde APITechU"});
  }
);

app.get("/apitechu/v1/users",
  function(req, res) {
    console.log("GET /apitechu/v1/users");
    res.sendFile('usuarios.json', {root:__dirname});
  }
);


app.post("/apitechu/v1/users",
  function(req, res) {
        console.log("POST  /apitechu/v1/users");
        //console.log(req.headers);
        //console.log("first_name is " + req.headers.first_name);
        //console.log("last_name is " + req.headers.last_name);
        //console.log("country is " + req.headers.country);
        console.log("first_name is " + req.body.first_name);
        console.log("last_name is " + req.body.last_name);
        console.log("country is " + req.body.country);

        var newUser = {
            //"first_name" : req.headers.first_name,
            //"last_name" : req.headers.last_name,
            //"country" : req.headers.country
            "first_name" : req.body.first_name,
            "last_name" : req.body.last_name,
            "country" : req.body.country
        };

        var users =require('./usuarios.json');
      // users pasa a ser de tipo array
        users.push(newUser);
        writeUserDataToFile(users);
        console.log("Usuario guardado con éxito");
        res.send({"msg" : "Usuario guardado con éxito"});
      //res.send(users);
      //console.log("Usuario añadido con éxito");
  }
);

app.delete("/apitechu/v1/users/:id",
  function(req, res) {
        console.log("DELETE /apitechu/v1/users/:id");
        console.log(req.params);
        console.log(req.params.id);

        var users = require('./usuarios.json');
        users.splice(req.params.id - 1, 1);
        writeUserDataToFile(users);
        console.log("Usuario borrado");
        res.send({"msg": "Usuario borrado"});
        }
)


function writeUserDataToFile(data){
  var fs =require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./usuarios.json",  jsonUserData, "utf8",
      function(err) {
        if(err) {
          //var msg = "Error al escribir fichero usuarios";
          console.log(err);
        } else {
          var msg = "Usuario persistido";
          console.log("Datos escritos en archivo");
        }

      }
    )
}

app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req, res) {
        console.log("Parámetros");
        console.log(req.params);
        console.log("Query");
        console.log(req.query);
        console.log("Headers");
		    console.log(req.headers);
        console.log("Body");
		    console.log(req.body);
          }
  )



    app.post("/apitechu/v1/login",
      function(req, res) {
            console.log("POST  /apitechu/v1/login");
            console.log("user is " + req.body.email);
            console.log("password is " + req.body.password);

            var users =require('./usuarios.json');
            var bolSuccess = false;
            var msg ="";
            var idUsuario="";
            var oSalida;

            for (user of users) {
	               if (user.email == req.body.email){
                   if (user.password == req.body.password){
                      idUsuario = user.id
                      user.logged = true;
	                    bolSuccess = true;
                    }
                 }
            }

            if (bolSuccess == true) {
                msg = "Login correcto"
                oSalida = {"mensaje" : msg,
                          "idUsuario" : idUsuario};
                writeUserDataToFile(users);
            } else {
                msg = "Login incorrecto"
                oSalida = {"mensaje" : msg};
            }


              console.log(msg);
              res.send(oSalida);

        }
      );

    app.post("/apitechu/v1/logout",
    function(req, res) {
          console.log("POST  /apitechu/v1/logout");
          console.log("user is " + req.body.id);


          var users =require('./usuarios.json');
          var bolSuccess = false;
          var msg ="";
          var idUsuario="";
          var oSalida;

          for (user of users) {
               if (user.id == req.body.id){
                 if (user.logged == true){
                    idUsuario = user.id
                    delete user.logged;
                    bolSuccess = true;
                  }
               }
          }

          if (bolSuccess == true) {
              msg = "Logout correcto"
              oSalida = {"mensaje" : msg,
                        "idUsuario" : idUsuario};
              writeUserDataToFile(users);
          } else {
              msg = "Logout incorrecto"
              oSalida = {"mensaje" : msg};
          }


            console.log(msg);
            res.send(oSalida);

      }
    );


    app.get("/apitechu/v2/users",
      function(req, res) {
        console.log("GET /apitechu/v2/users");

        var httpClient = requestJson.createClient(baseMLabURL);
        console.log("Cliente HTTP creado");

        httpClient.get("user?" + mLabAPIKey,
          function(err, resMLab, body) {
            var response = !err ? body : {
              "msg" : "Error obteniendo usuarios"
            }
            res.send(response);
          }
        )
      }
    );


    app.get("/apitechu/v2/users/:id",
      function(req, res) {
        console.log("GET /apitechu/v2/users/:id");

        var id = req.params.id;
        var query = 'q={"id":' + id + '}';

        var httpClient = requestJson.createClient(baseMLabURL);
        console.log("Cliente HTTP creado");

        httpClient.get("user?" + query + "&" + mLabAPIKey,
          function(err, resMLab, body) {
            // var response = !err ? body : {
            //  "msg" : "Error obteniendo usuario."
            // }
            var response = {};

            if (err) {
                       response = {
                            "msg" : "Error obteniendo usuario."
                        };
                        res.status(500);
                      } else {
                        if (body.length > 0) {
                          response = body;
                        } else {
                          response = {
                              "msg" : "Usuario no encontrado"
                          };
                        res.status(404);
                        }
                      }
          res.send(response);
          }
        )
      }
    );




app.post("/apitechu/v2/login",
function(req, res) {
    console.log("POST  /apitechu/v2/login");
    console.log("user is " + req.body.email);
    console.log("password is " + req.body.password);


    var email = req.body.email;
    var password = req.body.password;

    var query = 'q={"email":"' + email + '", "password":"' + password +'"}';

    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente HTTP creado");

    httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err, resMLab, bodyGet) {

        var response = {};

        if (err) {
                   response = {
                        "msg" : "Error obteniendo usuario."
                    };
                    res.status(500);
                    res.send(response);
                  } else {
                    if (bodyGet.length > 0) {
                      console.log(bodyGet[0]);

                      var putBody = '{"$set":{"logged":true}}';
                      httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                        function(err, resMLab, body) {
                          console.log("dentro put");
                          if (err) {
                             response = {
                                  "msg" : "Error logando usuario."
                              };
                              res.status(500);
                              res.send(response);
                          } else {
                              console.log(body);
                              response = {
                                   "msg" : "Usuario correctamente logado.",
                                   "idUsuario" : bodyGet[0].id,
                               };
                              console.log("conseguido!!");
                              res.send(response);
                          }
                        }
                      ) // fin: httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                      //response = body;
                    } else { // else: if (body.length > 0) {
                      response = {
                          "msg" : "Usuario o Password Incorrecto"
                      };
                    res.status(404);
                    res.send(response);
                    }
                  }
                  console.log("response: " + response);
                  //res.send(response);
      }
    )

  }
);

app.post("/apitechu/v2/logout",
  function(req, res) {
    console.log("POST  /apitechu/v2/logout");
    console.log("user is " + req.body.id);
    var id = req.body.id;
    var query = 'q={"id":' + id + '}';
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente HTTP creado");
    httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {

        var response = {};

        if (err) {
          response = {
            "msg" : "Error obteniendo usuario."
          };
          res.status(500);
          res.send(response);
        } else { // init get sin error
          if (body.length = 1) {
            console.log(body);
            console.log(body[0].logged);
            if (typeof body[0].logged == 'undefined') {
              console.log("if");
              response = {
                "msg" : "Error el usuario no se encuentra logado"
              };
              res.send(response);
            } else {
              console.log("else");
              var putBody = '{"$unset":{"logged":""}}'
              httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                function(err, resMLab, body) {
                  console.log("dentro put");
                  if (err) {
                    response = {
                      "msg" : "Error haciendo logout"
                    };
                    res.status(500);
                    res.send(response);
                  } else {
                    console.log(body);
                    response = body;
                    console.log("conseguido");
                    res.send(response);
                    //res.send(response);
                  } // end: function(err, resMLab, body)
                }
              ) // end: put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody)
            } // end: (typeof body[0].logged == 'undefined') {
          //response = body;
          } else { // else: if (body.length = 1) {
            response = {
            "msg" : "Error haciendo logout. Usuario no encontrado"
            };
            res.status(404);
            res.send(response);
          } // end: if (body.length = 1) {
        } // end get sin error
      console.log("response")
      //
      }          // function(err, resMLab, body)
    )
  }
)


app.get("/apitechu/v2/users/:id/accounts",
  function(req, res) {
    console.log("GET /apitechu/v2/users/:id/accounts");

    var id = req.params.id;
    var query = 'q={"userid":' + id + '}';

    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente HTTP creado");

    httpClient.get("account?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
        // var response = !err ? body : {
        //  "msg" : "Error obteniendo usuario."
        // }
        var response = {};

        if (err) {
                   response = {
                        "msg" : "Error obteniendo cuenta de usuario."
                    };
                    res.status(500);
                  } else {
                    if (body.length > 0) {
                      //response = body[0];
                      response = body;
                    } else {
                      response = {
                          "msg" : "Usuario no encontrado en Colección de cuentas"
                      };
                    res.status(404);
                    }
                  }
      res.send(response);
      }
    )
  }
);
